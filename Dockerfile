FROM debian:latest

ARG TF_VERSION=1.3.6
ARG TG_VERSION=0.42.3
ARG UID=1000

RUN apt-get update \
    && apt-get upgrade -y \
    && apt-get install -y unzip wget curl git-core zsh \
    && rm -rf /usr/share/doc/* /var/cache/apt/* /var/lib/apt/lists/*

WORKDIR /tmp/install
RUN wget https://releases.hashicorp.com/terraform/${TF_VERSION}/terraform_${TF_VERSION}_linux_amd64.zip -O tf.zip \
    && unzip tf.zip \
    && wget https://github.com/gruntwork-io/terragrunt/releases/download/v${TG_VERSION}/terragrunt_linux_amd64 -O terragrunt \
    && chmod +x terraform terragrunt \
    && mv terraform terragrunt /usr/local/bin \
    && rm -f tf.zip

RUN adduser --shell /bin/zsh --uid ${UID} --disabled-password \
    --gecos "ATF Trainee,,,," atf
