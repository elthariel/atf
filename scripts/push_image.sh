#! /usr/bin/env bash

image=registry.gitlab.com/elthariel/atf
tag=latest

here=$(dirname $0)

docker build -t "$image:$tag" -f "$here/../Dockerfile" "$here/.."

docker push "$image:$tag"
